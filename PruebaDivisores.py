'''
Created on 9 oct. 2020

@author: Cesar
'''
from Corr import C

class MetodoRecursivo:
    def divisores(self, n, m):
        if(m>0):
            if(n%m==0):
                print(m)
            self.divisores(n, m-1)
            
mr = MetodoRecursivo()
print("Progrma que muestra los divisores de un numero dado")
print("Ingresa el numero:")
n = C.correccion(C)
print("Los divisores de " + str(n) + " son: ")
mr.divisores(n, n)